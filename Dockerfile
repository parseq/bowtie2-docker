FROM ubuntu:14.04

MAINTAINER "Viktor Svekolkin" <enmce@yandex.ru>

RUN apt-get update && apt-get install -y \
	wget \
	zip \
	python \
	perl

#Download Bowtie2 binary	
RUN wget -P /opt http://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.2.6/bowtie2-2.2.6-linux-x86_64.zip

WORKDIR /opt
#Unzip Bowtie2 and remove archive after that
RUN unzip bowtie2-2.2.6-linux-x86_64.zip && \
	rm /opt/bowtie2-2.2.6-linux-x86_64.zip

WORKDIR /opt/bowtie2-2.2.6



