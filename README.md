#Bowtie2 docker image

This repository contains Dockerfile that packages Bowtie2 2.2.6 Docker image.

Bowtie2 documentation can be found [here](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml).
Bowtie2 can be downloaded [here](http://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.2.6/)

### The recommended way to build an image is:

```
sudo docker build -t bowtie2:2.2.6 .
```

### To run Bowtie2 2.2.6 in container:
Let`s say we want to index our reference genome.
If reference file <reference_name>.fasta is placed on hosts /work_dir, following command will produce result index <name> in hosts ~/work_dir.
```
docker run -v ~/work_dir/bowtie2-docker:/mnt/ -u $(echo $UID) --rm parseq/bowtie2:2.2.6 ./bowtie2-build -f /mnt/<reference_name>.fasta /mnt/<reference_name>_index

```
Let`s for example align single end reads <reads>.fastq which placed in ~/work_dir to our reference:
```
docker run -v ~/work_dir/bowtie2-docker:/mnt/ -u $(echo $UID) --rm parseq/bowtie2:2.2.6 ./bowtie2 -x /mnt/<reference_name>_index -U /mnt/<reads>.fastq -S /mnt/<reads>_alignments.sam
```
Resulting alignment <reads>_alignments.sam will be found in ~/work_dir.
